#!/usr/bin/env python3

## Z.Wells
## Made for the Academic Library at the University of Virginia's College at Wise
## October 2022

"""This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. """
    
# TODO: Implement infinity timeout

# Configurable Web crawler which recursively follows links on webpages and returns the response code from HTTP GET requests.

from sys import stderr
from sys import stdout
from socket import gaierror
import argparse
import http.client

## Parse arguments, and create helpfile
def parse_args():
    parser = argparse.ArgumentParser()
    
    group = parser.add_mutually_exclusive_group(required=True)
    
    group.add_argument(
        "-u",
        "--url",
        type=str,
        nargs='+',
        help="URL(s) with which to start recursively scanning",
        dest="url",
    )
    
    group.add_argument(
        "-f",
        "--file",
        type=str,
        nargs=1,
        default=[' '],
        help="Read URLs from a file",
        dest="file",
    )
    
    parser.add_argument(
        "-o",
        "--only-one",
        action="store_true",
        help="Prevent crawler from looking for links on webpages",
        dest="only_one",
    )
    
    parser.add_argument(
        "-i",
        "--infinite",
        action="store_true",
        help="Tell crawler to infinitely recurse through links (slow, may break)",
        dest="infinite",
    )
    
    parser.add_argument(
        "-l",
        "--logfile",
        type=str,
        nargs=1,
        default=["stdout"],
        help="File to record responses from web-pages in",
        dest="log",
    )
    
    return parser.parse_args()


# Open file with error handling
def open_file(file):
    try:
        lfile = open(file)
    except PermissionError:
        print("%s: Unable to open file: Permission denied", file=stderr)
    except IsADirectoryError:
        print("%s: Unable to open file: Is a directory", file=stderr)

    return lfile


## Performs get request, and returns response codes and data
def get_request(url, root_domain, method):
    split_url = url.split('/')

    if "http" in split_url[0]:
        domain = split_url[split_url.index('')+1]
    else:
        domain = root_domain
    
    if split_url[0] == "http:":
        conn = http.client.HTTPConnection(domain)
    else:
        conn = http.client.HTTPSConnection(domain)
    
    try:
        if "".join(split_url[2:]) == domain or url == domain:
            conn.request(method, "/")
        else:
            conn.request(method, url)
    except gaierror:
        print("Host not found.", file=stderr)
        exit(1)
                  
    response = conn.getresponse()
    data = repr(response.read())

    conn.close()

    return (response, data)


## Find all the URLs on this page
def parse_response(response, data):
    child_urls = []
    
    if response.status in range(400, 500):
        print("URL does not lead to data.")
        return child_urls;
    
    split_data = data.split('\"')
    
    for i in range(0, len(split_data)):
        if "href=" in split_data[i]:
            child_urls.append(split_data[i+1])
    
    return child_urls
    
        
## Follow a whole list of URLs provided
def follow_links(urls, response_file, root_domain, infinite, used_links):
    for url in urls:
        if infinite == False:
            info = get_request(url, root_domain, "HEAD")
            response = info[0]
            print(url, ":", response.status, response.reason, file=response_file)
        elif url not in used_links:
            used_links.append(url)
            parse_and_follow_parent(url, response_file, False, infinite, used_links)
            
# Perform a get request and run function to parse the response, if flag is set, go further
def parse_and_follow_parent(parent_url, response_file, no_follow, infinite, used_links):
    split_url = parent_url.split('/')
    
    try:
        domain = split_url[split_url.index('')+1]
    except ValueError:
        domain = split_url[0]

    info = get_request(parent_url, domain, "GET")
    response = info[0]
    data = info[1]
    
    # Record responses into file
    print("ROOT:", parent_url, ":", response.status, response.reason, file=response_file)
    
    if no_follow == False:
        new_urls = parse_response(response, data)
        follow_links(new_urls, response_file, domain, infinite, used_links)
    
    
def main():
    args = parse_args()
    parent_urls = args.url
    response_file = args.log[0]
    url_file = args.file[0]
    no_follow = args.only_one
    infinite = args.infinite
    
    if response_file == "stdout":
        response_file = stdout
    else:
        response_file = open_file(response_file)
    
    used_links = []
    
    if url_file != ' ':
        read_url_file = open_file(url_file)
        urls = [ url.strip('\n') for url in read_url_file ]
    else:
        urls = [ url.strip('\n') for url in args.url ]
        
    for url in urls:
        parse_and_follow_parent(url, response_file, no_follow, infinite, used_links)
    
    
# Only run main() if script is run directly
if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print()
        print("Bye.")

    exit(0)            
